var axios = require('axios');
var express = require('express');
require('dotenv').config()


const app = express();
app.use(express.static(__dirname + '/'));
app.use(express.urlencoded({extended: false}));

app.set('view engine', 'ejs');

app.post('/consulta', (req, res) =>{
    let config = {
        method: 'get',
        url: process.env.URL_API+`${req.body.txtCaso}`,
        headers: { 
          'Authorization': 'Basic '+process.env.AUTORIZATION
        }
      };

    axios(config)
    .then(function (response) {
        res.render('index', {caso: response.data.key, 
            estado: response.data.fields.status.name, 
            asignado: response.data.fields.assignee.displayName,
            resumen: response.data.fields.summary,
            ultimoComentario: lastComment(response)})        
    })
    .catch(function (error) {
        res.render('index', {caso: 'NO EXISTE', 
        estado: 'NO EXISTE', 
        asignado: 'ExcelCredit',
        resumen: 'Somos una Fintech, 100% colombiana',
        ultimoComentario: 'Ultimo comentario de tu consulta'})
        console.log(error)
    });
})

app.get('/', (req, res) =>{
    res.render('index', {caso: 'CASO-123456', 
        estado: 'CERRADO', 
        asignado: 'ExcelCredit',
        resumen: 'Somos una Fintech, 100% colombiana',
        ultimoComentario: 'Ultimo comentario de tu consulta'})
})

app.listen(8080, ()=>{
    console.log("puerto 8080")
})


const lastComment = (response) =>{
    let comentarios = response.data.fields.comment.comments;
    if (comentarios.length > 0) {
        let lastComment = comentarios[comentarios.length -1].body.content
        let comentario = '';
        lastComment.forEach(element => {
            if (element.type == 'paragraph') {
                element.content.forEach(x => {
                    if (x.text != undefined) {
                        comentario += `${x.text} `
                    }
                })
            }
        });
        return comentario;
    }else{
        return 'No existe comentarios para mostrar'
    }
}

